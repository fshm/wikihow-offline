import requests
from bs4 import BeautifulSoup

def get_url(url):#Function get_url
    response = requests.get(url)    #requesting for url from the external function
    return(response.text)    #it will return 

def get_links(html):
    output_list = []
    soup = BeautifulSoup(html,'html.parser')
    for link in soup.find_all("a"):
        if 'href' in link.attrs:
            if link.attrs['href'].startswith("#") or link.attrs['href'].startswith("/Special"):
                pass
            else:
                output_list.append(link.attrs.get('href'))
    return output_list

url = "https://www.wikihow.com/"
output_html = get_url(url)
output_links = get_links(output_html)

print(output_links)


